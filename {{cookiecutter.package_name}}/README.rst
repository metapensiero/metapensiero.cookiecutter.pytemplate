.. -*- coding: utf-8 -*-
.. :Project:   {{ cookiecutter.package_name }} -- {{ cookiecutter.package_description }}
.. :Created:   {{ cookiecutter.timestamp }}
.. :Author:    {{ cookiecutter.author_fullname }} <{{cookiecutter.author_email }}>
.. :License:   {{ cookiecutter.license }}
.. :Copyright: {{ cookiecutter.copyright }}
..

={{ cookiecutter.package_name|count * "=" }}=
 {{ cookiecutter.package_name }}
={{ cookiecutter.package_name|count * "=" }}=

{{ cookiecutter.package_description }}
{{ cookiecutter.package_description|count * "=" }}

:author: {{ cookiecutter.author_fullname }}
:contact: {{ cookiecutter.author_email }}
:license: {{ cookiecutter.license }}
